﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/IntersectionHighlights"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Color("Main Color", Color) = (1,1,1,1)
		_Color2("Main Color 2", Color) = (1,1,1,1)
	}
		SubShader
		{
			Tags { "Queue" = "Transparent" "RenderType" = "Transparent"  }

			Pass
			{
				Blend SrcAlpha OneMinusSrcAlpha
				ZWrite Off
				Cull Off

				CGPROGRAM
				#pragma target 3.0
				#pragma vertex vert 
				#pragma fragment frag
				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
					float3 normal : NORMAL;
				};

				struct Input {
					float2 uv_MainTex;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float2 screenuv : TEXCOORD1;
					float3 viewDir : TEXCOORD2;
					float3 objectPos : TEXCOORD3;
					float4 vertex : SV_POSITION;
					float depth : DEPTH;
					float3 normal : NORMAL;
				};

				sampler2D _CameraDepthNormalsTexture;
				fixed4 _Color;
				float4 _Color2;

				sampler2D _MainTex;
				float4 _MainTex_ST;

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = TRANSFORM_TEX(v.uv, _MainTex);

					o.screenuv = ((o.vertex.xy / o.vertex.w) + 1) / 2;
					o.screenuv.y = 1 - o.screenuv.y;
					o.depth = -mul(UNITY_MATRIX_MV, v.vertex).z *_ProjectionParams.w;

					return o;
				}

				half4 frag(v2f i) : COLOR
				{
					float screenDepth = DecodeFloatRG(tex2D(_CameraDepthNormalsTexture, i.screenuv).zw);
					float diff = screenDepth - i.depth;
					float intersect = 0;

					half4 main_color = lerp(_Color, _Color2, i.uv.x);

					if (diff > 0)
						intersect = 1 - smoothstep(0, _ProjectionParams.w * 0.5, diff);

					fixed4 c = main_color + intersect;

					return c;
				}

				ENDCG
			}
		}
			FallBack "VertexLit"
}