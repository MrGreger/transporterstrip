﻿using GameAnalyticsSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnalyticsInitializer : MonoBehaviour
{
    private void Awake()
    {
        GameAnalytics.Initialize();
    }
}
