﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputHandler : MonoBehaviour
{
    public UnityEvent TapDown;
    public UnityEvent TapUp;

    private void Start()
    {
        Camera.main.depthTextureMode = DepthTextureMode.Depth;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            TapDown?.Invoke();
        }

        if (Input.GetMouseButtonUp(0))
        {
            TapUp?.Invoke();
        }
    }

    private void OnDisable()
    {
        TapUp?.Invoke();
    }
}
