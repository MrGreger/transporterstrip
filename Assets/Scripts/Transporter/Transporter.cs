﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum TaransporterSpeed
{
    Normal = 0,
    Fast = 1,
    Slow = -1
}

public class Transporter : MonoBehaviour
{
    public UnityEventFloat SpeedChanged;

    [SerializeField]
    [Range(0, 4)]
    private float _speed = 1f;
    [SerializeField]
    [Range(0, 4)]
    private float _slowSpeed = .1f;
    [SerializeField]
    [Range(0, 4)]
    private float _fastSpeed = .1f;
    [SerializeField]
    private TaransporterSpeed _transporterSpeed;
    private Material _material;

    [SerializeField]
    private List<Product> _products = new List<Product>();

    private float _xOffset;

    [Space]
    [Header("For adjustment")]
    [SerializeField]
    [Range(1, 10)]
    private float _objectsSpeedModifier = 4f;

    private bool _stopped;

    public void ChangeSpeed(int speed)
    {
        _transporterSpeed = (TaransporterSpeed)speed;

        switch (_transporterSpeed)
        {
            case TaransporterSpeed.Normal:
                SpeedChanged?.Invoke(1);
                break;
            case TaransporterSpeed.Fast:
                break;
            case TaransporterSpeed.Slow:
                SpeedChanged?.Invoke(_speed + (_speed / _slowSpeed));
                break;
            default:
                break;
        }
    }

    public void AddProduct(Product product)
    {
        _products.Add(product);
    }

    public void RemoveProduct(Product product)
    {
        _products.Remove(product);
    }

    public void Clear()
    {
        for (int i = 0; i < _products.Count; i++)
        {
            Destroy(_products[i].gameObject);
        }

        _products.Clear();
    }

    public void Toggle(bool stopped)
    {
        _stopped = stopped;
    }

    private void Start()
    {
        _material = GetComponent<MeshRenderer>().material;
    }

    private void Update()
    {
        if (_stopped)
        {
            return;
        }

        Animate();

        _objectsSpeedModifier = transform.localScale.x / _material.GetTextureScale("_MainTex").x;

        var moveDir = transform.right;
        var moveDelta = moveDir * GetTranporterSpeed() * Time.deltaTime * _objectsSpeedModifier;

        for (int i = 0; i < _products.Count; i++)
        {
            _products[i].transform.position += moveDelta;
        }
    }

    private void Animate()
    {
        _xOffset += Time.deltaTime * GetTranporterSpeed();
        if (_xOffset > 1)
        {
            _xOffset -= 1;
        }
        _material.mainTextureOffset = new Vector2(_xOffset, 0);
    }

    private float GetTranporterSpeed()
    {
        if (_transporterSpeed == TaransporterSpeed.Slow)
        {
            return _slowSpeed;
        }

        if(_transporterSpeed == TaransporterSpeed.Fast)
        {
            return _fastSpeed;
        }

        return _speed;
    }
}
