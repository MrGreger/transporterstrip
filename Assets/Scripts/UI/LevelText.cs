﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class LevelText : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _text;

    [SerializeField]
    private string _levelCaption;
    [SerializeField]
    private string _bonusCaption;

    private string _caption;

    private int _level;
    private LevelType _levelType;

    public void OnLevelTypeChanged(LevelType levelType)
    {
        _levelType = levelType;

        if (levelType == LevelType.Normal)
        {
            _caption = _levelCaption;
        }
        else
        {
            _caption = _bonusCaption;
        }

        OnLevelChanged();
    }

    public void LevelCountChanged(int currentLevel)
    {
        _level = currentLevel;
        OnLevelChanged();
    }

    private void OnLevelChanged()
    {
        if (_levelType == LevelType.Bonus)
        {
            _text.text = _caption;
        }
        else
        {
            _text.text = $"{_caption}{_level}";
        }
    }
}
