﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScanProgress : MonoBehaviour
{
    [SerializeField]
    private Slider _slider;
    private int _currentValue;

    [SerializeField]
    private float _animationTime = 0.3f;

    public void Setup(int maxValue)
    {
        _slider.maxValue = maxValue;
        _slider.value = 0;
    }

    public void SetValue(int value)
    {
        StartCoroutine(SetNewValue(value));
    }

    private IEnumerator SetNewValue(int newValue)
    {
        var timer = 0f;

        while (timer < _animationTime)
        {
            _slider.value = Mathf.Lerp(_slider.value, newValue, timer / _animationTime);
            timer += Time.deltaTime;
            yield return null;
        }
    }
}
