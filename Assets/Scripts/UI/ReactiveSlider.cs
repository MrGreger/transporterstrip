﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class ReactiveSlider : MonoBehaviour
{
    private Slider _slider;

    public void OnValueChanged(float maxValue, float currentValue)
    {
        if(_slider == null)
        {
            _slider = GetComponent<Slider>();
        }

        _slider.maxValue = maxValue;
        _slider.value = currentValue;
    }

    public void OnValueChanged(int maxValue, int currentValue)
    {
        if (_slider == null)
        {
            _slider = GetComponent<Slider>();
        }

        _slider.maxValue = maxValue;
        _slider.value = currentValue;
    }
}
