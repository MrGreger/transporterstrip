﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(TextMeshProUGUI))]
public class ReactiveText : MonoBehaviour
{
    private TextMeshProUGUI _text;
    [SerializeField]
    private string _caption;

    public void OnValueChanged(int maxValue, int currentValue)
    {
        if (_text == null)
        {
            _text = GetComponent<TextMeshProUGUI>();
        }

        var captionAddition = string.IsNullOrWhiteSpace(_caption) ? "" : $"{_caption}: ";
        _text.text = $"{captionAddition}{currentValue}/{maxValue}";
    }
}
