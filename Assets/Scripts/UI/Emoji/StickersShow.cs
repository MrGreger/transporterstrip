﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickersShow : MonoBehaviour
{
    [SerializeField]
    private EmojiStorage _emojiStorage;
    [SerializeField]
    private Scanner _scanner;
    [SerializeField]
    private Transform _emojiContainer;
    [SerializeField]
    private GameObject _stickerPrefab;
    [SerializeField]
    [Range(0, 100)]
    private float _showInterval = 1;

    private Coroutine _showStickersCoroutine;
    private WaitForSeconds _waitForSeconds;

    public void ShowStickers(bool show)
    {
        if (!show)
        {
            if (_showStickersCoroutine != null)
            {
                StopCoroutine(_showStickersCoroutine);
                _showStickersCoroutine = null;
            }

            return;
        }

        _waitForSeconds = new WaitForSeconds(_showInterval);

        _showStickersCoroutine = StartCoroutine(ShowStickers());
    }

    public void ClearStickers()
    {
        for (int i = 0; i < _emojiContainer.childCount; i++)
        {
            var child = _emojiContainer.GetChild(i);
            Destroy(child.gameObject);
        }
    }

    private IEnumerator ShowStickers()
    {
        while (true)
        {
            CreateEmoji();
            yield return _waitForSeconds;
        }
    }

    private void CreateEmoji()
    {
        if(_scanner.CurrentScanEmotion == null)
        {
            return;
        }

        var sprite = _emojiStorage.GetEmoji(_scanner.CurrentScanEmotion.Value);

        var stickerDisplay = Instantiate(_stickerPrefab, _emojiContainer).GetComponent<StickerDisplay>();

        stickerDisplay.SetupEmoji(sprite);
    }
}
