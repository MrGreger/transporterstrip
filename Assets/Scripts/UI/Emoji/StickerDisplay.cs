﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class StickerDisplay : MonoBehaviour
{
    [SerializeField]
    private Image _image;
    [SerializeField]
    private RectTransform _rectTransform;
    [Range(.1f, 10f)]
    [SerializeField]
    private float _fadeTime = 1f;
    [Range(.1f, 50f)]
    public float _speed = 3f;

    private float _deathTimer = 0f;

    public void SetupEmoji(Sprite emojiSprite, bool randomPosition = true)
    {
        if (!randomPosition)
        {
            _rectTransform.anchoredPosition = Vector2.zero;
        }
        else
        {
            _rectTransform.anchoredPosition = GetRandomPostion();
        }

        _image.sprite = emojiSprite;
        _deathTimer = 0;
    }

    private Vector2 GetRandomPostion()
    {
        var parentRect = transform.parent.GetComponent<RectTransform>();
        var width = parentRect.rect.width;

        var availablePositions = width / 2 - _rectTransform.sizeDelta.x / 2;

        var randomPos = Random.Range(-availablePositions, availablePositions);

        return new Vector2(randomPos, Random.Range(0, _rectTransform.sizeDelta.y));
    }

    private void Update()
    {
        _deathTimer += Time.deltaTime;

        MoveUp();

        ChangeAlpha();

        if (_deathTimer >= _fadeTime)
        {
            Hide();
        }
    }

    private void ChangeAlpha()
    {
        var newColor = _image.color;
        var alpha = Mathf.Lerp(1, 0, _deathTimer / _fadeTime);
        newColor.a = alpha;
        _image.color = newColor;
    }

    private void MoveUp()
    {
        var position = _rectTransform.anchoredPosition;
        var newPosition = position + Vector2.up * _speed * Time.deltaTime;
        _rectTransform.anchoredPosition = newPosition;
    }

    private void Hide()
    {
        Destroy(gameObject);
    }
}
