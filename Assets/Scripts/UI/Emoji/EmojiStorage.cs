﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EmojiSet
{
    public ScanEmotion ScanAccuracy;
    public List<Sprite> EmojiSprites;
}

public class EmojiStorage : MonoBehaviour
{
    [SerializeField]
    private List<EmojiSet> _emojiSets;

    public Sprite GetEmoji(ScanEmotion scanEmotion)
    {
        EmojiSet set = null;

        for (int i = 0; i < _emojiSets.Count; i++)
        {
            if (_emojiSets[i].ScanAccuracy == scanEmotion)
            {
                set = _emojiSets[i];
                break;
            }
        }

        if (set == null)
        {
            Debug.LogError($@"Emoji set for accuracy ""{scanEmotion.ToString()}"" not found.");
            return _emojiSets[0].EmojiSprites[0];
        }

        return set.EmojiSprites[Random.Range(0, set.EmojiSprites.Count)];
    }
}
