﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GradientToggler : MonoBehaviour
{
    [SerializeField]
    private Gradient _gradientValues;
    [SerializeField]
    private List<Image> _imagesToToggle;

    public void ToggleGradient(float maxValue, float currentValue)
    {
        var color = _gradientValues.Evaluate(currentValue / maxValue);

        for (int i = 0; i < _imagesToToggle.Count; i++)
        {
            _imagesToToggle[i].color = color;
        }
    }
}
