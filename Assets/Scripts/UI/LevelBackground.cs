﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class LevelBackground : MonoBehaviour
{
    [SerializeField]
    private Image _backgroundImage;

    [SerializeField]
    private Sprite _normalBackground;
    [SerializeField]
    private Sprite _bonusSprite;

    public void OnLevelTypeChanged(LevelType levelType)
    {
        _backgroundImage.sprite = levelType == LevelType.Bonus ? _bonusSprite : _normalBackground;
    }
}
