﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SpawnMode
{
    Frenzy,
    BonusFrenzy,
    Normal,
    Bonus
}

public class ProductSpawner : MonoBehaviour
{
    [Range(0, 10)]
    [SerializeField]
    private float _spawnMinTime = 1;
    private float SpawnMinTime => _spawnMinTime * _timerScale;

    [Range(0, 10)]
    [SerializeField]
    private float _spawnMaxTime = 3;
    private float SpawnMaxTime => _spawnMaxTime * _timerScale;

    [SerializeField]
    private ProductsStorage _productsStorage;
    [SerializeField]
    private Transporter _transporter;

    [SerializeField]
    [Range(0, 100)]
    private int _badSpawnChance;
    [SerializeField]
    private int _maxGoodSpawn;
    private int _goodSpawned = 0;

    private float _timerScale = 1;

    private float _timer = 0;
    private float Timer => _timer * _timerScale;
    private float _timeToSpawn;
    private float TimeToSpawn => _timeToSpawn * _timerScale;

    private bool _spawn;

    public SpawnMode SpawnMode { get; private set; } = SpawnMode.Normal;

    public void ChangeTimer(float timerScale)
    {
        _timerScale = timerScale;
    }

    public void StopSpawning()
    {
        _spawn = false;
    }

    public void ResumeSpawning()
    {
        _spawn = true;
    }

    public void StartSpawning()
    {
        _spawn = true;
        _timeToSpawn = Random.Range(SpawnMinTime, SpawnMaxTime);
        _timer = 0;
    }

    public void OnLevelTypeChanged(LevelType levelType)
    {
        SpawnMode = levelType == LevelType.Bonus ? SpawnMode.Bonus : SpawnMode.Normal;
    }

    public void SetSpawningMode(SpawnMode spawnMode)
    {
        SpawnMode = spawnMode;
    }

    private void Start()
    {
        Spawn();
        StartSpawning();
    }

    public void Update()
    {
        if (!_spawn)
        {
            return;
        }

        _timer += Time.deltaTime;

        if (Timer >= TimeToSpawn)
        {
            Spawn();

            _timeToSpawn = Random.Range(SpawnMinTime, SpawnMaxTime);

            _timer = 0;
        }
    }

    private void Spawn()
    {
        Product product = null;

        var isBadProduct = Random.Range(0, 1f) <= (_badSpawnChance / 100f);

        if (!isBadProduct && _goodSpawned++ >= _maxGoodSpawn)
        {
            isBadProduct = true;
        }

        if (isBadProduct && SpawnMode == SpawnMode.Normal)
        {
            _goodSpawned = 0;
            product = _productsStorage.GetRandomBadProduct();
        }
        else if (SpawnMode == SpawnMode.Bonus || SpawnMode == SpawnMode.BonusFrenzy)
        {
            product = _productsStorage.GetRandomBonusProduct();
        }
        else
        {
            product = _productsStorage.GetRandomProduct();
        }

        product.transform.position = transform.position;

        _transporter.AddProduct(product);
    }
}
