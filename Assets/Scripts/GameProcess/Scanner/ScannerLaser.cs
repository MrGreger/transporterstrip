﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum ScannerColor
{
    Success,
    Normal,
    Bad
}

[System.Serializable]
public class ScannerColorSetting
{
    [SerializeField]
    private ScannerColor _scannerColor;
    [SerializeField]
    private Color _bottomColor;
    [SerializeField]
    private Color _topColor;
    public Color TopColor => _topColor;
    public ScannerColor ScannerColor => _scannerColor;
    public Color BottomColor => _bottomColor;
}

public class ScannerLaser : MonoBehaviour
{
    [SerializeField]
    private GameObject _graphics;
    private Material _graphicsMaterial;

    [SerializeField]
    private ScannerColorSetting[] _scannerColorSettings;

    private ScannerColor? _currentScannerColor;

    public void Show(bool successColor = false)
    {
        _graphics.SetActive(true);
    }

    public void Hide()
    {
        SetColor(ScannerColor.Normal);
        _graphics.SetActive(false);
    }

    public void SetColor(ScannerColor color)
    {
        if (_graphicsMaterial == null)
        {
            _graphicsMaterial = _graphics.GetComponent<MeshRenderer>().material;
        }

        if (_currentScannerColor != null && _currentScannerColor.Value == color)
        {
            return;
        }

        SetScannerColors(_scannerColorSettings.First(x => x.ScannerColor == color));
    }

    private void SetScannerColors(ScannerColorSetting scannerColorSetting)
    {
        _graphicsMaterial.SetColor("_Color", scannerColorSetting.TopColor);
        _graphicsMaterial.SetColor("_Color2", scannerColorSetting.BottomColor);
    }
}
