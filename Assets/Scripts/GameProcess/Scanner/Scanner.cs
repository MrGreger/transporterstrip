﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum ScanEmotion
{
    NotOnproduct,
    Good,
    Nice,
    BadProductScanning
}

public enum ScannerMode
{
    Default,
    Frenzy
}

public class Scanner : MonoBehaviour
{
    public UnityEventProduct ProductScanned;
    public UnityEvent ScanStart;
    public UnityEvent ScanEnd;

    public UnityEventFloatFloat EnergyChanged;

    public ScanEmotion? CurrentScanEmotion => _currentScanEmotion;

    [SerializeField]
    private ScannerLaser _scannerEffect;

    private bool _scanning;
    [Tooltip("Value from 0 to 1 - percent of edge length from center")]
    [Range(0, 1)]
    [SerializeField]
    private float _accuracyDistance = .5f;

    [SerializeField]
    private ScanEmotion? _currentScanEmotion;

    [Range(0.1f, 10f)]
    private float _scanTime = 1.4f;
    private float _scanTimer = 0;
    private bool _scanStarted = false;
    private Product _lastScannedProduct;

    [SerializeField]
    private LayerMask _produtsMask;

    private float _scanEnergy;
    [SerializeField]
    private float _energyPerSecond;

    [SerializeField]
    [Range(0f, 100f)]
    private float _badProductScanPerSecond;

    [SerializeField]
    private GameStateStorage _gameStateStorage;
    [SerializeField]
    private LevelSettingStorage _levelSettingStorage;

    private ScannerMode _scannerEnergyMode;

    private float _currentEnergy;
    public float CurrentEnergy
    {
        get => _currentEnergy;
        private set
        {
            _currentEnergy = value;
            EnergyChanged?.Invoke(_scanEnergy, _currentEnergy);
        }
    }

    public void ResetScannerEnergy()
    {
        _scanEnergy = _levelSettingStorage.GetLevelSetting(_gameStateStorage.State.Level).ScannerEnergy;
        CurrentEnergy = _scanEnergy;
    }

    public void SetScannerEnergyMode(ScannerMode mode)
    {
        _scannerEnergyMode = mode;
    }

    public void EnableScanner()
    {
        if (CurrentEnergy <= 0)
        {
            CurrentEnergy = 0;
            return;
        }

        _scannerEffect.Show();
        _scanning = true;
        ScanStart?.Invoke();
    }

    public void DisableScanner()
    {
        _scannerEffect.Hide();
        _scanning = false;
        ScanEnd?.Invoke();
    }

    public void ChangeScannerValue(int amount)
    {
        if (_scannerEnergyMode == ScannerMode.Frenzy)
        {
            if (amount > 0)
            {
                return;
            }
        }

        CurrentEnergy += amount;
        if (CurrentEnergy < 0)
        {
            CurrentEnergy = 0;
        }

        if (CurrentEnergy > _scanEnergy)
        {
            CurrentEnergy = _scanEnergy;
        }
    }

    public void Update()
    {
        if (!_scanning)
        {
            _currentScanEmotion = null;
            StopScan();
            return;
        }

        if (CurrentEnergy <= 0)
        {
            CurrentEnergy = 0;
            DisableScanner();
            return;
        }

        var ray = new Ray(transform.position, -transform.up * 1000);

        Product scannedProduct = null;

        if (Physics.Raycast(ray, out var hit, LayerMask.NameToLayer("Products")))
        {
            if (hit.transform.TryGetComponent<Product>(out var product))
            {
                scannedProduct = product;

                SetScannerColor(product);

                TryScan(product);
                CheckAccurasy(hit, product);
            }
            else
            {
                SetScannerColor(null);
                _currentScanEmotion = ScanEmotion.NotOnproduct;
            }
        }
        else
        {
            StopScan();
            SetScannerColor(null);
        }

        if (scannedProduct is BadProduct)
        {
            CurrentEnergy -= _badProductScanPerSecond * Time.deltaTime;
        }
        else
        {
            CurrentEnergy -= _energyPerSecond * Time.deltaTime;
        }

    }

    private void SetScannerColor(Product product)
    {
        if (product == null)
        {
            _scannerEffect.SetColor(ScannerColor.Normal);
            return;
        }

        if (product == _lastScannedProduct)
        {
            if (product is BadProduct)
            {
                _scannerEffect.SetColor(ScannerColor.Bad);
            }
            else
            {
                _scannerEffect.SetColor(ScannerColor.Success);
            }
        }
    }

    private void CheckAccurasy(RaycastHit hit, Product product)
    {
        var collider = product.GetComponent<Collider>();

        Vector2 center2D = new Vector2(collider.bounds.center.x, collider.bounds.center.z);
        Vector2 hitPoint2D = new Vector2(hit.point.x, hit.point.z);

        Debug.DrawLine(center2D, hitPoint2D, Color.red);

        float successDistance = collider.bounds.size.x * _accuracyDistance / 2;

        if (product is BadProduct)
        {
            _currentScanEmotion = ScanEmotion.BadProductScanning;
        }
        else
        {
            if (Vector2.Distance(center2D, hitPoint2D) <= successDistance)
            {
                _currentScanEmotion = ScanEmotion.Nice;
            }
            else
            {
                _currentScanEmotion = ScanEmotion.Good;
            }
        }
    }

    private void TryScan(Product product)
    {
        if (!_scanStarted)
        {
            if (_lastScannedProduct == product)
            {
                return;
            }

            StartScan();
        }

        _scanTimer += Time.deltaTime;

        if (_scanTimer >= product.ScanTime || _scannerEnergyMode == ScannerMode.Frenzy)
        {
            _lastScannedProduct = product;
            OnScanned();
        }
    }

    private void OnScanned()
    {
        ProductScanned?.Invoke(_lastScannedProduct);
        StopScan();
    }

    private void StopScan()
    {
        _scanStarted = false;
        _scanTimer = 0f;
    }

    private void StartScan()
    {
        _scanStarted = true;
        _scanTimer = 0f;
        _scannerEffect.SetColor(ScannerColor.Normal);
    }
}
