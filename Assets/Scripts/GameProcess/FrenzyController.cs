﻿using GameAnalyticsSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FrenzyController : MonoBehaviour
{
    [SerializeField]
    private GameObject _autoScanArea;
    [SerializeField]
    private Transporter _transporter;

    [SerializeField]
    private ProductSpawner _productSpawner;

    [SerializeField]
    private Scanner _scanner;

    private SpawnMode _lastSpawnMode;

    private System.Action _onFrenzyEnd;

    [SerializeField]
    private GameObject _frenzyVFX;
    [SerializeField]
    private ParticleSystem _frenzyVFXPS;

    [SerializeField]
    private float _frenzySpeed;

    [SerializeField]
    private GameObject _frenzyText;

    public void StartFrenzy(System.Action onFrenzyEnd, LevelType levelType)
    {
        var badProducts = FindObjectsOfType<BadProduct>();

        for (int i = 0; i < badProducts.Length; i++)
        {
            _transporter.RemoveProduct(badProducts[i]);
            Destroy(badProducts[i].gameObject);
        }

        _scanner.SetScannerEnergyMode(ScannerMode.Frenzy);
        _frenzyText.SetActive(true);

        _frenzyVFXPS.Play();
        _autoScanArea.SetActive(true);
        _lastSpawnMode = _productSpawner.SpawnMode;
        _productSpawner.SetSpawningMode(levelType == LevelType.Normal ? SpawnMode.Frenzy : SpawnMode.BonusFrenzy);
        Time.timeScale = _frenzySpeed;
        _onFrenzyEnd = onFrenzyEnd;
    }

    public void OnScannerEnergy(float maxEnergy, float currentEnergy)
    {
        if (currentEnergy <= 0)
        {
            _scanner.SetScannerEnergyMode(ScannerMode.Default);

            _frenzyVFXPS.Stop();
            _frenzyText.SetActive(false);

            _autoScanArea.SetActive(false);
            Time.timeScale = 1;
            _productSpawner.SetSpawningMode(_lastSpawnMode);
            _onFrenzyEnd?.Invoke();
            _onFrenzyEnd = null;
        }
    }
}
