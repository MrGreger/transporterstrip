﻿using GameAnalyticsSDK;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public enum LevelType
{
    Normal,
    Bonus
}

public class Game : MonoBehaviour
{
    public UnityEventProductProuct GameEnd;
    public UnityEventIntInt ProgressChanged;
    public UnityEventIntInt FailedProductsCountChanged;
    public UnityEvent Win;
    public UnityEvent Loose;
    public UnityEvent GameStarted;
    public UnityEvent GoodProductScanned;
    public UnityEvent BadProductScanned;
    public UnityEventLevelType LevelTypeChanged;
    public UnityEventInt LevelChagned;


    public Product FirstScannedProduct { get; private set; }
    public Product LastScannedProduct { get; private set; }

    [Range(1, 100)]
    [SerializeField]
    private int _productsToWin = 4;
    private int _productsScanned;

    [SerializeField]
    private ScanProgress _scanProgress;

    [SerializeField]
    private GameStateStorage _gameStateStorage;
    [SerializeField]
    private LevelSettingStorage _levelSettingStorage;

    [SerializeField]
    [Range(0, 100)]
    private int _bonusLevelChance = 50;
    [SerializeField]
    private int _maxNormalLevelsInARow = 2;

    private LevelType _levelType;

    [SerializeField]
    private FrenzyController _frenzyController;

    private bool _frenzy;

    private void Start()
    {
        _levelType = _gameStateStorage.State.CurrentLevelType ?? LevelType.Normal;

        LevelTypeChanged?.Invoke(_levelType);
        LevelChagned?.Invoke(_gameStateStorage.State.Level);
    }

    public void StartGame()
    {
        _frenzy = false;

        _productsScanned = 0;

        var levelSettings = _levelSettingStorage.GetLevelSetting(_gameStateStorage.State.Level);

        _productsToWin = levelSettings.ProductsToWin;

        _scanProgress.Setup(_productsToWin);

        if (_levelType == LevelType.Normal)
        {
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, $"Level {_gameStateStorage.State.Level} started.");
        }
        else
        {
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, $"Bonus started.");
        }

        ProgressChanged?.Invoke(_productsToWin, _productsScanned);
        GameStarted?.Invoke();
        LevelTypeChanged?.Invoke(_levelType);
    }

    public void OnProductScanned(Product product)
    {
        LastScannedProduct = product;

        if (product is BadProduct)
        {
            OnProductFailed(product);
            BadProductScanned?.Invoke();
            return;
        }

        GoodProductScanned?.Invoke();

        if (_productsScanned == 0)
        {
            FirstScannedProduct = product;
        }

        _productsScanned++;

        ProgressChanged?.Invoke(_productsToWin, _productsScanned);

        _scanProgress.SetValue(_productsScanned);

        if (_productsToWin == _productsScanned)
        {
            OnWin();
        }
    }

    private void OnWin()
    {
        Win?.Invoke();

        if (_levelType == LevelType.Normal)
        {
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, $"Level {_gameStateStorage.State.Level} completed.");
        }
        else
        {
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, $"Bonus level completed");
        }


        _frenzy = true;

        _frenzyController.StartFrenzy(() =>
        {
            if (_levelType == LevelType.Normal)
            {
                var state = _gameStateStorage.State;
                state.Level++;
                state.NormalLevelsInARow++;
                LevelChagned?.Invoke(state.Level);
                _gameStateStorage.SetState(state);
            }

            GameEnd?.Invoke(FirstScannedProduct, LastScannedProduct);

            SetNextLevelType();
        }, _levelType);
    }

    private void SetNextLevelType()
    {
        //_gameStateStorage.State.CurrentLevelType

        var bonusLevel = Random.Range(0, 1f) <= _bonusLevelChance / 100f;

        if (!bonusLevel)
        {
            if (_gameStateStorage.State.NormalLevelsInARow.HasValue)
            {
                bonusLevel = _gameStateStorage.State.NormalLevelsInARow.Value > _maxNormalLevelsInARow;
            }
        }

        _levelType = bonusLevel ? LevelType.Bonus : LevelType.Normal;

        var state = _gameStateStorage.State;

        if (bonusLevel)
        {
            state.NormalLevelsInARow = 0;
        }

        state.CurrentLevelType = _levelType;

        _gameStateStorage.SetState(state);
    }

    public void OnScannerChanged(float maxValue, float currentValue)
    {
        if (_frenzy)
        {
            return;
        }

        if (currentValue <= 0)
        {
            if (_levelType == LevelType.Normal)
            {
                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, $"Level {_gameStateStorage.State.Level} failed.");
            }
            else
            {
                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, $"Bonus level failed.");
            }

            Loose?.Invoke();
        }
    }

    public void OnProductFailed(Product product)
    {

    }

    private void OnDrawGizmos()
    {
        if (FirstScannedProduct != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawCube(FirstScannedProduct.transform.position, Vector3.one);
        }
    }
}
