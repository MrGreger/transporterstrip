﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AutoScanArea : MonoBehaviour
{
    public UnityEvent OnScanAreaEnter;
    public UnityEvent OnScanAreaExit;

    private void OnTriggerEnter(Collider other)
    {
        OnScanAreaEnter?.Invoke();
    }

    private void OnTriggerExit(Collider other)
    {
        OnScanAreaExit?.Invoke();
    }
}
