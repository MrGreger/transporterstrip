﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
[System.Serializable]
public class GameState
{
    public int Level;
    [SerializeField]
    public int? NormalLevelsInARow = null;
    [SerializeField]
    public LevelType? CurrentLevelType = null;
}

public class GameStateStorage : MonoBehaviour
{
    public GameState State { get; private set; }

    private void Start()
    {
        if (!PlayerPrefs.HasKey("GameState"))
        {
            State = new GameState();
            State.Level = 1;
            SaveToPlayerPrefs();
        }
        else
        {
            var stateToLoad = PlayerPrefs.GetString("GameState");
            State = JsonConvert.DeserializeObject<GameState>(stateToLoad);
        }

    }

    private void SaveToPlayerPrefs()
    {
        var stateToSave = JsonConvert.SerializeObject(State);
        print(stateToSave);
        PlayerPrefs.SetString("GameState", stateToSave);
        PlayerPrefs.Save();
    }

    public void SetState(GameState state)
    {
        State = state;
        SaveToPlayerPrefs();
    }
}

