﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FailChecker : MonoBehaviour
{
    private Product _currentProduct;
    [SerializeField]
    private Game _game;

    public UnityEventProduct ProductFailed;

    private bool _enabled = false;

    public void Enable()
    {
        _enabled = true;
    }

    public void Disable()
    {
        _enabled = false;
    }

    private void OnTriggerExit(Collider other)
    {
        if (!_enabled)
        {
            return;
        }

        if (other.TryGetComponent<Product>(out var product))
        {
            if (_game.LastScannedProduct != product && !product.Dropped)
            {
                DropProduct(product);

                if (!(product is BadProduct))
                {
                    ProductFailed?.Invoke(product);
                }
            }
        }
    }

    private void DropProduct(Product product)
    {
        product.Drop();
    }
}
