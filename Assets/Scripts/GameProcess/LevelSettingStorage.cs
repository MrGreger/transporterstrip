﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class LevelSetting
{
    [SerializeField]
    private int _level;
    public int Level => _level;
    [SerializeField]
    [Range(0,100)]
    private int _productsToWin;
    public int ProductsToWin => _productsToWin;
    [SerializeField]
    [Range(0, 100)]
    private float _scannerEnergy;
    public float ScannerEnergy => _scannerEnergy;
}

public class LevelSettingStorage : MonoBehaviour
{
    [SerializeField]
    private LevelSetting[] _levelSettings;

    public LevelSetting GetLevelSetting(int level)
    {
        return _levelSettings.FirstOrDefault(x => level <= x.Level) ?? _levelSettings.Last();
    }
}
