﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductsStorage : MonoBehaviour
{
    [SerializeField]
    private List<ProductInfo> _products;
    [SerializeField]
    private List<ProductInfo> _bonusLevelProducts;
    [SerializeField]
    private List<ProductInfo> _badProducts;

    public Product GetRandomProduct()
    {
        var productInfo = _products[Random.Range(0, _products.Count)];
        var product = Instantiate(productInfo.Product.gameObject).GetComponent<Product>();
        product.SetScanTime(productInfo.ScanTime);
        return product;
    }

    public Product GetRandomBonusProduct()
    {
        var productInfo = _bonusLevelProducts[Random.Range(0, _bonusLevelProducts.Count)];
        var product = Instantiate(productInfo.Product.gameObject).GetComponent<Product>();
        product.SetScanTime(productInfo.ScanTime);
        return product;
    }

    public Product GetRandomBadProduct()
    {
        var productInfo = _badProducts[Random.Range(0, _badProducts.Count)];
        var product = Instantiate(productInfo.Product.gameObject).GetComponent<Product>();
        product.SetScanTime(productInfo.ScanTime);
        return product;
    }
}
