﻿using UnityEngine;

[CreateAssetMenu(fileName = "new product", menuName = "products/create new")]
public class ProductInfo : ScriptableObject
{
    public Product Product;
    public float ScanTime;
}

