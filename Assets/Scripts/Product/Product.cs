﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class Product : MonoBehaviour
{
    private Rigidbody _rigidbody;
    private Collider _collider;

    public bool Dropped { get; private set; }
    public float ScanTime { get; private set; }

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _collider = GetComponent<Collider>();
        Dropped = false;
    }

    public void SetScanTime(float scanTime)
    {
        ScanTime = scanTime;
    }

    public void Drop()
    {
        Dropped = true;

        _rigidbody.isKinematic = false;
        _rigidbody.useGravity = true;

        Debug.DrawRay(transform.position, (transform.forward + transform.up * .5f), Color.red);

        _rigidbody.AddForce((transform.forward + transform.up) * 5f, ForceMode.Impulse);
        _rigidbody.AddTorque((transform.forward + transform.up) * 5f, ForceMode.Impulse);

        _collider.isTrigger = true;
    }
}
