﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class DepthHelper : MonoBehaviour
{
    public Camera Camera;

    private void OnEnable()
    {
        this.Camera.depthTextureMode = DepthTextureMode.DepthNormals;
    }

    private void Start()
    {
        this.Camera.depthTextureMode = DepthTextureMode.DepthNormals;
    }
}
