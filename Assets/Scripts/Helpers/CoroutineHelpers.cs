﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class CoroutineHelpers
{
    public static void StartCoroutine(this MonoBehaviour gameObject, IEnumerator coroutine, Action afterComplete)
    {
        gameObject.StartCoroutine(CoroutineWithCallback(coroutine, afterComplete));
    }

    private static IEnumerator CoroutineWithCallback(IEnumerator coroutine, Action callback)
    {
        yield return coroutine;

        callback?.Invoke();
    }
}

