﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CameraMover : MonoBehaviour
{
    public Transform CameraTransform;

    public UnityEvent Moved;

    private bool _moving;
    private Vector3 _target;
    private float _speed;
    private bool _onlyXMove;

    public void SetDestination(Vector3 point, float speed, bool onlyX = true)
    {
        _target = point;
        _speed = speed;
        _moving = true;
        _onlyXMove = onlyX;
    }

    private void Update()
    {
        if (!_moving)
        {
            return;
        }

        var lastPosition = CameraTransform.position;
        var nextPosition = Vector3.MoveTowards(CameraTransform.position, _target, _speed);

        if (_onlyXMove)
        {
            nextPosition.y = lastPosition.y;
            nextPosition.z = lastPosition.z;
        }

        CameraTransform.position = nextPosition;

        var dist = Mathf.Abs(CameraTransform.position.x - _target.x);

        if (dist <= 0.1f)
        {
            Moved?.Invoke();
            _moving = false;
        }
    }
}
