﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Waypoint
{
    public float Speed;
    public Transform Point;
}

public class CameraAnimator : MonoBehaviour
{
    [SerializeField]
    protected List<Waypoint> _waypoints;
    [SerializeField]
    protected CameraMover _cameraMover;

    protected int _currentWaypoint = 0;

    public virtual void ResetAnimator()
    {
        _currentWaypoint = 0;
    }

    public virtual void Step()
    {
        if (_currentWaypoint == _waypoints.Count)
        {
            return;
        }

        var waypoint = _waypoints[_currentWaypoint++];

        _cameraMover.SetDestination(waypoint.Point.position, waypoint.Speed);
    }
}