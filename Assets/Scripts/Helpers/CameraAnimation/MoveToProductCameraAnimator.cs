﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// VERY BAD DESIGN REFACTOR THIS INSTEAD MODIFYING
/// </summary>
public sealed class MoveToProductCameraAnimator : CameraAnimator
{
    [System.Serializable]
    private class DynamicWaypoint
    {
        public float Speed;
        [HideInInspector]
        public Transform Point;
        public UnityEvent Moved;
    }

    [SerializeField]
    private DynamicWaypoint _winWaypoint = new DynamicWaypoint();
    [SerializeField]
    private DynamicWaypoint _resetWaypoint = new DynamicWaypoint();

    private DynamicWaypoint _lastPlayed;

    public void OnGameEnd(Product firstScannedProduct, Product lastScannedProduct)
    {
        _winWaypoint.Point = firstScannedProduct.transform;
        _resetWaypoint.Point = lastScannedProduct.transform;
    }

    public void PlayWinAnimation()
    {
        SetupWaypoint(_winWaypoint);
        Step();
    }

    public void PlayResetAnimation()
    {
        SetupWaypoint(_resetWaypoint);
        Step();
    }

    public void OnAnimationEnd()
    {
        if (_lastPlayed == _winWaypoint)
        {
            _winWaypoint.Moved?.Invoke();
        }
        else if (_lastPlayed == _resetWaypoint)
        {
            _resetWaypoint.Moved?.Invoke();
        }
    }

    private void SetupWaypoint(DynamicWaypoint waypoint)
    {
        ResetAnimator();

        if(_waypoints == null)
        {
            _waypoints = new List<Waypoint>();
        }

        _waypoints.Clear();

        _lastPlayed = waypoint;

        _waypoints.Add(new Waypoint
        {
            Point = waypoint.Point,
            Speed = waypoint.Speed
        });
    }
}

