﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TapTracker : MonoBehaviour
{
    public UnityEvent ScreenTapped;

    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ScreenTapped?.Invoke();
        }
    }
}
