﻿using UnityEngine.Events;

[System.Serializable]
public class UnityEventProduct : UnityEvent<Product> { }

[System.Serializable]
public class UnityEventProductProuct : UnityEvent<Product, Product> { }


[System.Serializable]
public class UnityEventFloatFloat : UnityEvent<float, float> { }

[System.Serializable]
public class UnityEventFloat : UnityEvent<float> { }

[System.Serializable]
public class UnityEventIntInt : UnityEvent<int, int> { }

[System.Serializable]
public class UnityEventInt : UnityEvent<int> { }

[System.Serializable]
public class UnityEventLevelType : UnityEvent<LevelType> { }