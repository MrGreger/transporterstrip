﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ToStartPositionMover : MonoBehaviour
{
    public UnityEvent Moved;
    [SerializeField]
    private CameraMover _cameraMover;
    [SerializeField]
    private Waypoint _waypoint;

    public void Move()
    {
        _cameraMover.SetDestination(_waypoint.Point.position, _waypoint.Speed, false);
    }

    public void OnMoved()
    {
        Moved?.Invoke();
    }
}
